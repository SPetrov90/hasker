Hasker otus
===========

Q&A site, like stackoverflow (start with cookiecutter-django)

:License: MIT


Usage in production
--------------

Requirements
^^^^^^^^^^^^^^^^^^^^^

* docker

* docker-compose (v3)


Quick start
^^^^^^^^^^^^^^^^^^^^^
For run production server in docker environment::

    $ git clone https://gitlab.com/SPetrov90/hasker.git
    $ cd hasker
    $ docker-compose -f production.yml build
    $ docker-compose -f production.yml up


Set environment
^^^^^^^^^^^^^^^^^^^^^
For run app you must set environment variables in ``env_file``  ::

    envs
    ├── local
    │   ├── .django
    │   └── .postgres
    └── .production
        ├── .django
        └── .postgres

Consider file ``envs/.production/.postgres``: ::

    # PostgreSQL
    # ------------------------------------------------------------------------------
    POSTGRES_HOST=postgres
    POSTGRES_DB=<your project slug>
    POSTGRES_USER=<postgres user>
    POSTGRES_PASSWORD=<postgres user password>

Consider file ``envs/.production/.django``: ::

    # Django
    # ------------------------------------------------------------------------------
    DJANGO_SETTINGS_MODULE=<settings module>
    DJANGO_SECRET_KEY=<secret key>
    DJANGO_ADMIN_URL=<url for admin app>
    DJANGO_ALLOWED_HOSTS=<allow hosts>
    # Email
    # ------------------------------------------------------------------------------
    MAILGUN_API_KEY=<mailgun key>
    MAILGUN_DOMAIN=<mailgun accound domain>
    # Sentry
    # ------------------------------------------------------------------------------
    SENTRY_DSN=<yours sentry url>
    # Redis
    # ------------------------------------------------------------------------------
    REDIS_URL=<redis url, default: redis://redis:6379/0>
    # Flower
    CELERY_FLOWER_USER=<flower user>
    CELERY_FLOWER_PASSWORD=<flower password>


Execute command
^^^^^^^^^^^^^^^^^^^^^
For run database migrations and create admin user use:  ::

    $ docker-compose -f local.yml run --rm django python manage.py migrate
    $ docker-compose -f local.yml run --rm django python manage.py createsuperuser

Tests
^^^^^^^^^^^^^
For run the tests ::

    $ docker-compose -f local.yml run django pytest


Screenshot :)
^^^^^^^^^^^^

.. image:: https://pp.userapi.com/c845524/v845524815/e4e4a/uPJd2s-fhmA.jpg

Usage for development
--------------

Start
^^^^^^^^^^^^^
Build and run development docker container ::

    $ docker-compose -f production.yml build
    $ docker-compose -f production.yml up


Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Requirements:

* node

* npm

For starting change css styles use: ::

    $ npm install
    $ npm run build


Email Server
^^^^^^^^^^^^

In development, it is often nice to be able to see emails that are being sent from your application. For that reason local SMTP server Mailhug with a web interface is available as docker container.

Container mailhog will start automatically when you will run all docker containers.

With MailHog running, to view messages that are sent by your application, open your browser and go to ``http://127.0.0.1:8025``

API
--------------

Base
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Base ulr for API: ``http://localhost:8000/api/v1/``

Site has API for:

* question list (sort by popular)::

    http://localhost:8000/api/v1/questions/(?sort=top)


* popular question::

    http://localhost:8000/api/v1/questions/trending/

* question details::

    http://localhost:8000/api/v1/questions/<question_slug>/

* question answers::

    http://localhost:8000/api/v1/questions/<question_slug>/answers/

* search questions (by title and description)::

    http://localhost:8000/api/v1/questions/search/<query>/

* search questions (by tag)::

    http://localhost:8000/api/v1/questions/search/tag/<tag_name>/


Authorization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

API is provided only to the registered user on the basis of JSON Web Token Authentication.

* for getting token::

    $ curl -X POST -d "username=admin&password=password123" http://localhost:8000/api/v1/api-token-auth/

* for verify token::

    $ curl -X POST -d "username=admin&password=password123" http://localhost:8000/api/v1/api-token-verify/

* example API query with token::

    $ curl -H "Authorization: JWT <your_token>" http://localhost:8000//api/v1/<api_url>


Swagger
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* An API documentation with Swagger UI available on::

    http://localhost:8000/api/v1/docs/
