from django.core.exceptions import ValidationError
from django.forms import CharField, ModelForm, TextInput

from .models import Answer, Question
from .widgets import TagsWidget


class QuestionCreateForm(ModelForm):
    tags = CharField(
        label='Теги', required=True, widget=TagsWidget,
        help_text='Минимум 1, Максимум 3',
    )
    title = CharField(widget=TextInput, label='Вопрос', max_length=200)

    def clean_tags(self):
        value = self.cleaned_data['tags']
        if not value:
            raise ValidationError('Укажите по крайне мере один тег')
        value = self.cleaned_data['tags'].split(',')

        # дополнительная защита, по умолчанию ограничение на уровне js widget-a
        if len(value) > 3:
            raise ValidationError('Не может быть более трех тегов')
        return value

    class Meta:
        model = Question
        fields = ('title', 'text', 'tags')


class AnswerCreateForm(ModelForm):

    class Meta:
        model = Answer
        fields = ('text',)
