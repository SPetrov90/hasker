from django.contrib import admin

from .models import Answer, Question, Tag, Vote


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    pass


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'text', 'created_on', 'is_active')
    autocomplete_fields = ('tags',)
    list_display_links = ('id', 'title')
    list_per_page = 30
    search_fields = ('id', 'title',)
    ordering = ('created_on',)
    prepopulated_fields = {'slug': ('title',)}
    # inlines = [Answer]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    pass
