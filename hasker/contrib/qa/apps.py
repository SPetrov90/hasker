from django.apps import AppConfig


class QAAppConfig(AppConfig):

    name = "hasker.contrib.qa"
    verbose_name = "Вопрос-Ответ"
