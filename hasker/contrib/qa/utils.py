import datetime

from django.utils.six import text_type

DAY_ALTERNATIVES = {
    1: ("вчера", "завтра"),
    2: ("позавчера", "послезавтра"),
}  #: Day alternatives (i.e. one day ago -> yesterday)

DAY_VARIANTS = (
    "день",
    "дня",
    "дней",
)  #: Forms (1, 2, 5) for noun 'day'

HOUR_VARIANTS = (
    "час",
    "часа",
    "часов",
)  #: Forms (1, 2, 5) for noun 'hour'

MINUTE_VARIANTS = (
    "минуту",
    "минуты",
    "минут",
)  #: Forms (1, 2, 5) for noun 'minute'

PREFIX_IN = u"через"  #: Prefix 'in' (i.e. B{in} three hours)
SUFFIX_AGO = u"назад"  #: Prefix 'ago' (i.e. three hours B{ago})


def split_values(ustring, sep=u','):
    """
    Splits unicode string with separator C{sep},
    but skips escaped separator.

    @param ustring: string to split
    @type ustring: C{unicode}

    @param sep: separator (default to u',')
    @type sep: C{unicode}

    @return: tuple of splitted elements
    """
    assert isinstance(ustring, text_type), "uvalue must be unicode, not %s" % type(ustring)
    # unicode have special mark symbol 0xffff which cannot be used in a regular text,
    # so we use it to mark a place where escaped column was
    ustring_marked = ustring.replace(u'\,', u'\uffff')
    items = tuple([i.strip().replace(u'\uffff', u',') for i in ustring_marked.split(sep)])
    return items


def choose_plural(amount, variants):
    """
    Choose proper case depending on amount
    @param amount: amount of objects
    @type amount: C{integer types}
    @param variants: variants (forms) of object in such form:
        (1 object, 2 objects, 5 objects).
    @type variants: 3-element C{sequence} of C{unicode}
        or C{unicode} (three variants with delimeter ',')
    @return: proper variant
    @rtype: C{unicode}
    @raise ValueError: variants' length lesser than 3
    """

    if isinstance(variants, text_type):
        variants = split_values(variants)

    if len(variants) != 3:
        raise ValueError("должно быть три значения")
    amount = abs(amount)

    if amount % 10 == 1 and amount % 100 != 11:
        variant = 0
    elif amount % 10 >= 2 and amount % 10 <= 4 and \
            (amount % 100 < 10 or amount % 100 >= 20):
        variant = 1
    else:
        variant = 2

    return variants[variant]


def distance_of_time_in_words(from_time, accuracy=1):
    """

    :param from_time: datetime source
    :param accuracy: count max delta time periods
        Example :
        accuracy=1 -> пять дней назад
        accuracy=2 -> пять дней, 3 часа назад
        accuracy=3 -> пять дней, 3 часа, 15 минут назад

    :return: human readable timedelta in past or future (days, hours, minutes)
    """
    to_time = datetime.datetime.now()

    if accuracy not in [1, 2, 3]:
        raise ValueError("Допустимые значения для точности 1,2,3 сейчас: {}".format(str(accuracy)))

    if not isinstance(from_time, datetime.datetime):
        from_time = datetime.datetime.fromtimestamp(from_time)

    if from_time.tzinfo and not to_time.tzinfo:
        to_time = to_time.replace(tzinfo=from_time.tzinfo)

    dt_delta = to_time - from_time
    difference = dt_delta.days*86400 + dt_delta.seconds

    minutes_orig = int(abs(difference)/60.0)
    hours_orig = int(abs(difference)/3600.0)
    days_orig = int(abs(difference)/86400.0)

    in_future = from_time > to_time

    words = []
    values = []
    alternatives = []

    days = days_orig
    hours = hours_orig - days_orig*24
    minutes = minutes_orig - hours_orig * 60

    words.append("%d %s" % (days, choose_plural(days, DAY_VARIANTS)))
    values.append(days)

    words.append("%d %s" % (hours, choose_plural(hours, HOUR_VARIANTS)))
    values.append(hours)

    words.append(u"%d %s" % (minutes, choose_plural(minutes, MINUTE_VARIANTS)))
    values.append(minutes)

    days == 0 and hours == 1 and alternatives.append("час")
    days == 0 and hours == 0 and minutes == 1 and alternatives.append("минуту")

    # убираем из values и words конечные нули
    while values and not values[-1]:
        values.pop()
        words.pop()
    # убираем из values и words начальные нули
    while values and not values[0]:
        values.pop(0)
        words.pop(0)

    limit = min(accuracy, len(words))
    real_words = words[:limit]
    real_values = values[:limit]
    # снова убираем конечные нули
    while real_values and not real_values[-1]:
        real_values.pop()
        real_words.pop()
        limit -= 1

    real_str = " ".join(real_words)

    # альтернативные варианты нужны только если в real_words одно значение
    # и, вдобавок, если используется текущее время
    alter_str = limit == 1 and alternatives and \
        alternatives[0]
    _result_str = alter_str or real_str
    result_str = in_future and u"%s %s" % (PREFIX_IN, _result_str) \
        or u"%s %s" % (_result_str, SUFFIX_AGO)

    # если же прошло менее минуты, то real_words -- пустой, и поэтому
    # нужно брать alternatives[0], а не result_str
    zero_str = minutes == 0 and not real_words and \
        (
            in_future and "менее чем через минуту"
            or "менее минуты назад"
        )

    # нужно использовать вчера/позавчера/завтра/послезавтра
    # если days 1..2 и в real_words одно значение
    day_alternatives = DAY_ALTERNATIVES.get(days, False)
    alternate_day = day_alternatives and limit == 1 and \
        ((in_future and day_alternatives[1])
         or day_alternatives[0])

    final_str = not real_words and zero_str or alternate_day or result_str

    return final_str
