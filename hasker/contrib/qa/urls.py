from django.urls import path, register_converter

from .converters import NegativeIntConverter
from .views import (
    QuestionCorrectView,
    QuestionCreateView,
    QuestionDetails,
    RatingChangeView,
    SearchListView,
    answer_add,
    autocomplete_tags
)

register_converter(NegativeIntConverter, 'negint')

app_name = "qa"
urlpatterns = [
    path('add/', QuestionCreateView.as_view(), name='question-add'),
    path(
        'vote/<str:app_label>/<str:model_name>/<int:object_id>/<negint:action>/',
        RatingChangeView.as_view(), name='rating',
    ),
    path('search/', SearchListView.as_view(), name='search'),
    path('autocomplete/', autocomplete_tags, name='tags-autocomplete'),
    path('<int:id>/answer/add', answer_add, name='answer-add'),
    path('<slug:slug>/', QuestionDetails.as_view(), name='question-detail'),
    path('<int:question_id>-<int:answer_id>/correct/', QuestionCorrectView.as_view(), name='correct-answer'),
]
