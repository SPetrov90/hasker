from django import template
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string

from ..models import Question, Vote
from ..utils import distance_of_time_in_words

register = template.Library()


@register.simple_tag
def trending_questions():
    top_list = Question.objects.filter(is_active=True).order_by('-rating_sum')[:10]
    return render_to_string(
        'qa/tags/trending_questions.html', {
            'object_list': top_list,
        },
    )


@register.filter
def time_ago(value):
    return distance_of_time_in_words(value)


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()
    dict_[field] = value

    return dict_.urlencode()


@register.simple_tag
def rating(instance, user):
    ctype = ContentType.objects.get_for_model(instance)

    # get user choice
    choice = 0
    if user.is_authenticated:
        try:
            vote = Vote.objects.filter(content_type=ctype, object_id=instance.pk, user=user).get()
            choice = vote.vote
        except Vote.DoesNotExist:
            pass

    return render_to_string(
        'qa/tags/rating.html', {
            'user_choice': choice,
            'rating': instance.rating_sum,
            'app_label': ctype.app_label,
            'model': ctype.model,
            'instance_pk': instance.pk,
        },
    )
