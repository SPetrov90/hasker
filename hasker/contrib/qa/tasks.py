from celery import shared_task
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from .models import Answer, Question


@shared_task
def sent_answer_add_notification(answer_id, question_id):
    answer = Answer.objects.get(pk=answer_id)
    question = Question.objects.get(pk=question_id)
    user = answer.author
    request = None
    full_url = ''.join(['http://', get_current_site(request).domain, question.get_absolute_url()])

    ctx = {
        'user': user,
        'url': full_url,
    }

    text_content = render_to_string('email/answer_add.txt', ctx)
    html_content = render_to_string('email/answer_add.html', ctx)

    msg = EmailMultiAlternatives(
        'Новое сообщение',
        text_content,
        'hasker@noretry.ru',
        [user.email],
    )

    msg.attach_alternative(html_content, "text/html")
    msg.send()
