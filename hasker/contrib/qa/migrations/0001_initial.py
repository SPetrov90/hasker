# Generated by Django 2.0.8 on 2018-08-22 18:57
import datetime

import django.db.models.deletion
from django.conf import settings
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(help_text='Ответ не может быть более 2000 сисволов', max_length=2000, verbose_name='Ответ')),
                ('created_on', models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата создания')),
                ('is_correct', models.BooleanField(default=False, verbose_name='Правильный?')),
                (
                    'author', models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='answer_set', to=settings.AUTH_USER_MODEL, verbose_name='Автор',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Ответ на вопрос',
                'verbose_name_plural': 'Ответы на вопрос',
            },
        ),
        migrations.CreateModel(
            name='FavoriteItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата создания')),
                ('object_id', models.PositiveIntegerField()),
                (
                    'content_type', models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='+', to='contenttypes.ContentType',
                    ),
                ),
                (
                    'user', models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='favorites_set', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Избранное',
                'verbose_name_plural': 'Избранное',
                'ordering': ('-created_on',),
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(help_text='Вопрос не может быть более 200 сисволов', max_length=200, verbose_name='Вопрос')),
                ('text', models.TextField(max_length=2000, verbose_name='Описание вопроса')),
                ('slug', models.SlugField(max_length=100, unique=True, verbose_name='slug')),
                ('created_on', models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата создания')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='Последнее редактирование')),
                ('is_active', models.BooleanField(db_index=True, default=False, verbose_name='Опубликовано?')),
                (
                    'author', models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='question_set', to=settings.AUTH_USER_MODEL, verbose_name='Автор',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Вопрос',
                'verbose_name_plural': 'Вопросы',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
            ],
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vote', models.CharField(choices=[('NEU', 0), ('POS', 1), ('NEG', -1)], default='NEU', max_length=3)),
                ('object_id', models.BigIntegerField()),
                (
                    'content_type', models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='votes_set', to='contenttypes.ContentType',
                    ),
                ),
                (
                    'user', models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь',
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name='question',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='article_set', to='qa.Tag', verbose_name='Теги'),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='answer_set', to='qa.Question', verbose_name='Вопрос',
            ),
        ),
    ]
