# Generated by Django 2.0.8 on 2018-08-26 09:55
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ('-created_on',), 'verbose_name': 'Вопрос', 'verbose_name_plural': 'Вопросы'},
        ),
        migrations.AlterField(
            model_name='vote',
            name='vote',
            field=models.IntegerField(choices=[(0, 'NEUTRAL'), (1, 'POSITIVE'), (-1, 'NEGATIVE')], default=0),
        ),
    ]
