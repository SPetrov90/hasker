import datetime

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.text import slugify
from unidecode import unidecode

User = get_user_model()


class Question(models.Model):
    title = models.TextField('Вопрос', max_length=200, help_text='Вопрос не может быть более 200 сисволов')
    text = models.TextField('Описание вопроса', max_length=2000)

    author = models.ForeignKey(User, models.CASCADE, 'question_set', verbose_name='Автор', null=False)

    slug = models.SlugField('slug', max_length=100, unique=True)
    created_on = models.DateTimeField('Дата создания', default=datetime.datetime.now)
    updated_on = models.DateTimeField('Последнее редактирование', auto_now=True)
    is_active = models.BooleanField('Опубликовано?', default=False, db_index=True)
    tags = models.ManyToManyField('qa.Tag', 'article_set', verbose_name='Теги', blank=True)
    correct_answer = models.ForeignKey('qa.Answer', models.SET_NULL, '+', verbose_name='Правильный ответ', null=True)
    # save sum in database
    rating_sum = models.IntegerField('Рейтинг', default=0)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        ordering = ('-created_on',)
        get_latest_by = ('-created_on',)

    def _get_unique_slug(self):
        slug = slugify(unidecode(self.title))
        unique_slug = slug
        num = 1
        while Question.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('qa:question-detail', kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super(Question, self).save(*args, **kwargs)

    @cached_property
    def answer_count(self):
        return self.answer_set.count()

    def update_correct_answer(self, answer_id):
        if Question.objects.filter(pk=self.pk, correct_answer=answer_id).exists():
            Question.objects.filter(pk=self.pk).update(correct_answer=None)
        else:
            Question.objects.filter(pk=self.pk).update(correct_answer=answer_id)


class Answer(models.Model):
    text = models.TextField('Ответ', max_length=2000, help_text='Ответ не может быть более 2000 сисволов')
    question = models.ForeignKey(Question, models.CASCADE, 'answer_set', verbose_name='Вопрос')
    author = models.ForeignKey(User, models.CASCADE, 'answer_set', verbose_name='Автор', null=False)
    created_on = models.DateTimeField('Дата создания', default=datetime.datetime.now)
    # save sum in database
    rating_sum = models.IntegerField('Рейтинг', default=0)

    class Meta:
        verbose_name = 'Ответ на вопрос'
        verbose_name_plural = 'Ответы на вопрос'
        get_latest_by = ('-created_on',)

    def __unicode__(self):
        return self.text[:500]

    @cached_property
    def is_correct(self):
        if self.question.correct_answer and self.question.correct_answer.pk == self.pk:
            return True
        return False


class Tag(models.Model):
    name = models.CharField('Название', max_length=100)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Vote(models.Model):

    VOTE_CHOICES = (
        (0, 'NEUTRAL'),
        (1, 'POSITIVE'),
        (-1, 'NEGATIVE'),
    )

    vote = models.IntegerField(
        choices=VOTE_CHOICES,
        default=0,
    )

    user = models.ForeignKey(User, models.CASCADE, '+', verbose_name='Пользователь', null=False)

    content_type = models.ForeignKey(ContentType, models.PROTECT, 'votes_set')
    object_id = models.BigIntegerField()
    object_ct = GenericForeignKey('content_type', 'object_id')

    @staticmethod
    def is_valid_action_change_vote(value):
        return value in [-1, 1]

    @staticmethod
    def _get_correct_vote_value(value):
        # remove double actions
        if value in [1, 2]:
            return 1
        elif value in [-2, -1]:
            return -1
        elif value == 0:
            return value
        else:
            raise ValueError('incorrect vote value')

    @staticmethod
    def change_rating_action_vote(app_label, model_name, object_id, action, user):
        ct = ContentType.objects.get_by_natural_key(app_label, model_name)
        vote, _ = Vote.objects.get_or_create(content_type=ct, object_id=object_id, user=user)
        vote_value = Vote._get_correct_vote_value(vote.vote + action)
        vote.vote = vote_value
        vote.save()

        # get current object rating and update value in ct model
        rating = Vote.objects.filter(content_type=ct, object_id=object_id). \
            aggregate(models.Sum('vote')).get('vote__sum')
        model_class = ct.model_class()
        model_class.objects.filter(pk=object_id).update(rating_sum=rating)

        return vote_value, rating
