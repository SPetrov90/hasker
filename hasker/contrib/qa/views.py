import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.postgres.search import SearchVector
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db.models import Count
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import CreateView, View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .forms import AnswerCreateForm, QuestionCreateForm
from .models import Answer, Question, Tag, Vote
from .tasks import sent_answer_add_notification


class QuestionsListView(ListView):
    model = Question
    paginate_by = 10
    template_name = 'pages/home.html'

    def get_queryset(self):
        qs = self.model.objects.filter(is_active=True).\
            prefetch_related('author', 'tags', 'correct_answer').\
            defer('text').annotate(Count('answer_set', distinct=True))
        try:
            sort = self.request.GET.get('sort_by')
            if sort == 'popular':
                qs = qs.order_by('-rating_sum', '-created_on')
        except Exception:
            pass
        return qs


class QuestionDetails(DetailView):
    model = Question
    template_name = 'qa/question_detail.html'

    def get_context_data(self, **kwargs):
        context = super(QuestionDetails, self).get_context_data(**kwargs)
        qs = self.object.answer_set.all().order_by('-rating_sum', '-created_on')
        p = Paginator(qs, 10)
        page = 1
        try:
            page_num = int(self.request.GET.get('page'))
            if page_num <= p.num_pages:
                page = page_num
        except Exception:
            pass

        context['answers'] = p.get_page(page)

        if self.request.user.is_authenticated:
            context['answer_form'] = AnswerCreateForm()
            context['is_author'] = self.request.user.pk == self.object.author.pk

        return context

    def get_object(self, queryset=None):
        obj = super(QuestionDetails, self).get_object(queryset)
        if not obj.is_active:
            raise Http404
        return obj


class QuestionCreateView(LoginRequiredMixin, CreateView):
    template_name = 'qa/question_create.html'
    form_class = QuestionCreateForm
    login_url = 'account_login'

    def form_valid(self, form):
        tags_list = form.cleaned_data['tags']
        question = form.save(commit=False)
        question.author = self.request.user
        question.is_active = True
        question.save()
        for tag in tags_list:
            tag, _ = Tag.objects.get_or_create(name=tag)
            question.tags.add(tag)

        messages.success(self.request, 'Ваш вопрос успешно добавлен.')
        return redirect('qa:question-detail', slug=question.slug)


class QuestionCorrectView(View):

    http_method_names = ['post']

    def post(self, request, question_id, answer_id, *args, **kwargs):
        question = get_object_or_404(Question, pk=question_id, author=request.user)
        question.update_correct_answer(answer_id)
        return redirect('qa:question-detail', slug=question.slug)


def autocomplete_tags(request):
    query = request.GET.get('q', '')
    qs = {}
    if query:
        qs = Tag.objects.filter(name__icontains=query).values('id', 'name')
    return JsonResponse(list(qs), safe=False)


@login_required
def answer_add(request, *args, **kwargs):
    question = get_object_or_404(Question, pk=kwargs.get('id'))
    answer = Answer(author=request.user, question=question)
    form = AnswerCreateForm(request.POST, instance=answer)
    if form.is_valid():
        form.save()
        sent_answer_add_notification.delay(answer.pk, question.pk)

    return redirect('qa:question-detail', slug=question.slug)


class RatingChangeView(View):
    http_method_names = ['post']

    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            raise Http404
        return super(RatingChangeView, self).dispatch(request, *args, **kwargs)

    def post(self, request, app_label, model_name, object_id, action,  *args, **kwargs):
        try:
            if not request.user.is_authenticated:
                return HttpResponseForbidden('not authorized')

            # action only more or less
            if not Vote.is_valid_action_change_vote(action):
                return HttpResponseBadRequest('bad action')

            vote_value, rating = Vote.change_rating_action_vote(
                app_label,
                model_name,
                object_id,
                action,
                request.user,
            )
            response = {'status': 'ok', 'vote': vote_value, 'votes_all': rating}
            return HttpResponse(json.dumps(response))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest('bad request')


class SearchListView(ListView):
    model = Question
    paginate_by = 10
    template_name = 'pages/search.html'

    def get_queryset(self):
        qs = self.model.objects.filter(is_active=True).\
            prefetch_related('author', 'tags', 'correct_answer').\
            defer('text')

        query = self.request.GET.get('search').strip()
        if not query or len(query) > 70:
            messages.error(self.request, 'Поисковый запрос не может быть пустым или более 70 символов')
            return Question.objects.none()

        if query and query.startswith('tag:'):
            query = query[4:]
            search_vector = SearchVector('tags__name',)
        else:
            search_vector = SearchVector('title', 'text')
        qs = qs.annotate(search=search_vector).filter(search=query)
        return qs
