import json

from django import forms
from django.utils.safestring import mark_safe

from .models import Tag

AUTOCOMPLETE_URL = '/qa/autocomplete/'

TAG_CONFIG = {
    'allowNewTokens': True,
    'propertyToSearch': 'name',
    'hintText': 'начните вводить для поиска...',
    'noResultsText': 'такого тега еще нет...',
    'searchingText': 'поиск...',
    'creationText': 'Создать',
    'theme': 'facebook',
    'autocomplete': 'on',
    'tokenLimit': 3,
    'preventDuplicates': True,
}


class TagsWidget(forms.TextInput):

    class Media:
        css = {
            'all': ('css/token-input.css', 'css/token-input-facebook.css'),
        }
        js = ('js/jquery.tokeninput.js', 'js/tokeninput.initial.js')

    def render(self, name, value, attrs=None, renderer=None):
        """Render the widget as an HTML string."""
        config = TAG_CONFIG.copy()
        if value:
            # инициализируем значения тегов в виджете, если поле уже заполнено
            pre_populate = []
            value_list = value.split(',')
            for tag_name in value_list:
                tag = Tag.objects.get(name=tag_name)
                pre_populate.append({"id": tag.pk, "name": tag.name})
            config['prePopulate'] = pre_populate

        config_json = json.dumps(config)
        html = '<input type="text" ' \
               'name="tags" ' \
               'class="tagswidget ' \
               'form-control" id="id_{}" ' \
               'url=\'{}\'' \
               'config=\'{}\'">'.format(name, AUTOCOMPLETE_URL, config_json)
        return mark_safe(html)
