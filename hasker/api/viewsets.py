from django.contrib.postgres.search import SearchVector
from rest_framework import generics, status
from rest_framework.response import Response

from hasker.api.serializers import AnswerSerializer, QuestionSerializer, QuestionTrendingSerializer
from hasker.contrib.qa.models import Answer, Question


class QuestionListApiView(generics.ListAPIView):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        queryset = Question.objects.filter(is_active=True)

        ordering = self.request.query_params.get('sort')
        if ordering == 'top':
            queryset = queryset.order_by('-rating_sum', '-created_on')

        return queryset


class QuestionTrendingListApiView(generics.ListAPIView):
    serializer_class = QuestionTrendingSerializer

    def get_queryset(self):
        queryset = Question.objects.filter(is_active=True).order_by('-rating_sum', '-created_on')[:10]
        return queryset


class SearchApiView(generics.ListAPIView):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        query = self.kwargs.get('query').strip()
        if not query or len(query) > 70:
            return Response('search request more 70 letters', status=status.HTTP_400_BAD_REQUEST)
        qs = Question.objects.filter(is_active=True)
        search_vector = SearchVector('title', 'text')
        qs = qs.annotate(search=search_vector).filter(search=query)
        return qs


class SearchByTagApiView(generics.ListAPIView):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        query = self.kwargs.get('tag_name').strip()
        if not query or len(query) > 70:
            return Response('search request more 70 letters', status=status.HTTP_400_BAD_REQUEST)
        qs = Question.objects.filter(is_active=True)
        search_vector = SearchVector('tags__name',)
        qs = qs.annotate(search=search_vector).filter(search=query)
        return qs


class QuestionApiView(generics.RetrieveAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    lookup_field = 'slug'


class QuestionAnswersApiView(generics.ListAPIView):
    serializer_class = AnswerSerializer

    def get_queryset(self):
        slug = self.kwargs.get('slug')
        queryset = Answer.objects.filter(question__slug=slug)
        return queryset
