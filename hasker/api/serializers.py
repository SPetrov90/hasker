from rest_framework import serializers

from hasker.contrib.qa.models import Answer, Question


class QuestionSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    tags = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name',
    )
    answer_count = serializers.ReadOnlyField()
    rating = serializers.SerializerMethodField('get_rating_sum')

    class Meta:
        model = Question
        fields = ('title', 'text', 'slug', 'created_on', 'author', 'tags', 'rating', 'answer_count')

    def get_rating_sum(self, obj):
        return obj.rating_sum


class QuestionTrendingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ('title', 'rating_sum',)


class AnswerSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    rating = serializers.SerializerMethodField('get_rating_sum')

    class Meta:
        model = Answer
        fields = ('text', 'created_on', 'author', 'rating', 'is_correct')

    def get_rating_sum(self, obj):
        return obj.rating_sum
