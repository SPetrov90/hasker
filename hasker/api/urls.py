from django.conf.urls import include, url
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from .viewsets import (
    QuestionAnswersApiView,
    QuestionApiView,
    QuestionListApiView,
    QuestionTrendingListApiView,
    SearchApiView,
    SearchByTagApiView
)

app_name = 'api_1'

schema_view = get_swagger_view(title='Hasker API (swagger)')
router = DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    path('questions/', QuestionListApiView.as_view(), name='questions'),
    path('questions/trending/', QuestionTrendingListApiView.as_view(), name='questions_trending'),
    path('questions/search/<str:query>/', SearchApiView.as_view(), name='questions_search'),
    path('questions/search/tag/<str:tag_name>/', SearchByTagApiView.as_view(), name='questions_search_by_tag'),
    path('questions/<slug:slug>/', QuestionApiView.as_view(), name='questions_details'),
    path('questions/<slug:slug>/answers/', QuestionAnswersApiView.as_view(), name='questions_answers'),
    path('docs/', schema_view),
]
