from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse


class User(AbstractUser):

    avatar = models.ImageField('Логотип', upload_to='images/%Y/%m/', blank=True)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
