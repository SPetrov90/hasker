from django.contrib.auth import forms, get_user_model
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.forms import FileInput, ModelForm
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class ImageWidget(FileInput):
    """
    A ImageField Widget for admin that shows a thumbnail.
    Taken from https://djangosnippets.org/snippets/1580/
    """

    def render(self, name, value, attrs=None, **kwargs):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" style="height: 250px;" /></a> <br /><br />'
                           % (value.url, value.url)))
        output.append(super().render(name, value, attrs))
        return mark_safe(u''.join(output))


class UserUpdateForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].help_text = ''
        self.fields['avatar'].widget = ImageWidget()

    class Meta:
        model = User
        fields = ['username', 'email', 'avatar']

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']

        if avatar:
            try:
                w, h = get_image_dimensions(avatar)

                # validate dimensions
                max_width = max_height = 1000
                if w > max_width or h > max_height:
                    raise ValidationError(
                        'Please use an image that is '
                        '%s x %s pixels or smaller.' % (max_width, max_height),
                    )

                # validate content type
                main, sub = avatar.content_type.split('/')
                if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
                    raise ValidationError(
                        'Please use a JPEG, '
                        'GIF or PNG image.',
                    )

                # validate file size
                if len(avatar) > (200 * 1024):
                    raise ValidationError(
                        'Avatar file size may not exceed 200k.',
                    )

            except AttributeError:
                """
                Handles case when we are updating the user profile
                and do not supply a new avatar
                """
                pass

        return avatar


class UserChangeForm(forms.UserChangeForm):

    class Meta(forms.UserChangeForm.Meta):
        model = User


class UserCreationForm(forms.UserCreationForm):

    error_message = forms.UserCreationForm.error_messages.update(
        {"duplicate_username": _("This username has already been taken.")},
    )

    class Meta(forms.UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise ValidationError(self.error_messages["duplicate_username"])
