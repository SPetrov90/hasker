argon2-cffi==18.1.0  # https://github.com/hynek/argon2_cffi
celery==4.2.1  # pyup: <5.0  # https://github.com/celery/celery
coreapi==2.3.3  # https://github.com/core-api/python-client

# Django
# ------------------------------------------------------------------------------
django==2.0.8  # pyup: < 2.1  # https://www.djangoproject.com/
django-allauth==0.36.0  # https://github.com/pennersr/django-allauth
django-crispy-forms==1.7.2  # https://github.com/django-crispy-forms/django-crispy-forms
django-environ==0.4.5  # https://github.com/joke2k/django-environ
django-filter==1.1 # https://github.com/carltongibson/django-filter
django-model-utils==3.1.2  # https://github.com/jazzband/django-model-utils
django-redis==4.9.0  # https://github.com/niwinz/django-redis
django-rest-swagger==2.2.0 # https://github.com/marcgibbons/django-rest-swagger

# Django REST Framework
djangorestframework==3.8.2  # https://github.com/encode/django-rest-framework
djangorestframework-jwt==1.11.0 # https://github.com/GetBlimp/django-rest-framework-jwt/
flower==0.9.2  # https://github.com/mher/flower
Markdown==2.6.11 # https://github.com/Python-Markdown/markdown
Pillow==5.2.0  # https://github.com/python-pillow/Pillow
python-slugify==1.2.5  # https://github.com/un33k/python-slugify
pytz==2018.5  # https://github.com/stub42/pytz
redis>=2.10.5  # https://github.com/antirez/redis
