from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from tests.users.factories import UserFactory

from hasker.contrib.qa.models import Answer, Question, Tag, Vote


class TestQAQuestionsListView(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.user2 = UserFactory(username='test')

        # questions (4 all, 3 active)
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.question2 = Question.objects.create(title="Title2", text='text2', author=cls.user, is_active=True)
        cls.question2 = Question.objects.create(title="Title3", text='text2', author=cls.user, is_active=True)
        cls.question3 = Question.objects.create(title="Title3", text='text2', author=cls.user, is_active=False)

        # answers
        cls.answer = Answer.objects.create(text='answer1', author=cls.user, question=cls.question2)
        cls.answer2 = Answer.objects.create(text='answer2', author=cls.user, question=cls.question2)

        # tags
        cls.tag = Tag.objects.create(name='tag1')
        cls.tag2 = Tag.objects.create(name='tag2')
        cls.question2.tags.add(cls.tag)
        cls.question2.tags.add(cls.tag2)
        cls.question.tags.add(cls.tag2)

        # vote
        cls.vote = Vote.objects.create(user=cls.user, object_ct=cls.question2, vote=1)
        cls.vote = Vote.objects.create(user=cls.user2, object_ct=cls.question2, vote=1)
        Question.objects.filter(pk=cls.question2.pk).update(rating_sum=2)

    def setUp(self):
        self.client.login(username='login', password='TeStPaSS12345')

    @staticmethod
    def questions_list_api_url():
        return reverse('api_v1:questions')

    @staticmethod
    def questions_trending_api_url():
        return reverse('api_v1:questions')

    @staticmethod
    def questions_detail_api_url(slug):
        return reverse('api_v1:questions_details', kwargs={"slug": slug})

    @staticmethod
    def questions_answers_api_url(slug):
        return reverse('api_v1:questions_answers', kwargs={"slug": slug})

    @staticmethod
    def questions_search_api_url(text):
        return reverse('api_v1:questions_search', kwargs={"query": text})

    @staticmethod
    def questions_search_by_tag_api_url(tag):
        return reverse('api_v1:questions_search_by_tag', kwargs={"tag_name": tag})

    def test_questions_list_view(self):
        url = self.questions_list_api_url()
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_questions_list_top_view(self):
        url = self.questions_list_api_url() + '?sort=top'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)
        self.assertEqual(response.data['results'][0]['title'], self.question2.title)
        self.assertEqual(response.data['results'][0]['rating'], 2)

    def test_questions_trending_view(self):
        url = self.questions_detail_api_url(self.question2.slug)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], self.question2.title)
        self.assertEqual(response.data['rating'], 2)
        self.assertEqual(response.data['answer_count'], 2)

    def test_questions_answers_view(self):
        url = self.questions_answers_api_url(self.question2.slug)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_questions_search_view(self):
        url = self.questions_search_api_url('title3')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['title'], self.question3.title)

    def test_questions_search_by_tag_view(self):
        url = self.questions_search_by_tag_api_url('tag2')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['results'][0]['title'], self.question2.title)
        self.assertEqual(response.data['results'][1]['title'], self.question.title)
