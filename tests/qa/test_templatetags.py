from datetime import datetime, timedelta

from django.template import Context, Template
from django.test import TestCase
from tests.users.factories import UserFactory

from hasker.contrib.qa.models import Question


class TestQATemplateTags(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)

    def test_time_ago(self):

        c = Context({
            'now': datetime.now(),
            '10min': datetime.now() - timedelta(minutes=10),
            'hour': datetime.now() - timedelta(minutes=60),
            'day': datetime.now() - timedelta(days=1),
            '30days': datetime.now() - timedelta(days=30),
        })
        t = Template('{% load questions_tags %}{{ now|time_ago }}')
        self.assertEqual(t.render(c), 'менее минуты назад')

        t = Template('{% load questions_tags %}{{ 10min|time_ago }}')
        self.assertEqual(t.render(c), '10 минут назад')

        t = Template('{% load questions_tags %}{{ hour|time_ago }}')
        self.assertEqual(t.render(c), 'час назад')

        t = Template('{% load questions_tags %}{{ day|time_ago }}')
        self.assertEqual(t.render(c), 'вчера')

        t = Template('{% load questions_tags %}{{ 30days|time_ago }}')
        self.assertEqual(t.render(c), '30 дней назад')

    def test_trending_questions(self):
        c = Context()
        template_to_render = Template(
            '{% load questions_tags %}'
            '{% trending_questions %}',
        )
        rendered_template = template_to_render.render(c)
        self.assertInHTML('<h4>Топ вопросов</h4>', rendered_template)

    def test_rating(self):
        context = Context({
            'instance': self.question,
            'user': self.user,
        })
        template_to_render = Template(
            '{% load questions_tags %}'
            '{% rating instance user %}',
        )
        rendered_template = template_to_render.render(context)
        self.assertInHTML('<span class="ration-value">0</span>', rendered_template)
        self.assertIn('<a class="vote-up"', rendered_template)
        self.assertIn('<a class="vote-down"', rendered_template)
