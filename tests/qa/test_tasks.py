from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.test import TestCase
from tests.users.factories import UserFactory

from hasker.contrib.qa.models import Answer, Question
from hasker.contrib.qa.tasks import sent_answer_add_notification


class TestQATasks(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.answer = Answer.objects.create(text='text', author=cls.user, question=cls.question)

    def test_sent_answer_add_notification(self):
        sent_answer_add_notification(self.answer.pk, self.question.pk)

        message = mail.outbox[0]
        self.assertEqual(message.subject, 'Новое сообщение')
        self.assertEqual(message.from_email, 'hasker@noretry.ru')
        self.assertIn(self.question.author.email, message.to)
        self.assertEqual(len(message.alternatives), 1)
        self.assertEqual(message.alternatives[0][1], 'text/html')

        request = None
        full_url = ''.join(['http://', get_current_site(request).domain, self.question.get_absolute_url()])
        self.assertIn(full_url, message.body)
