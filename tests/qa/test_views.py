from unittest.mock import patch

from django.test import RequestFactory, TestCase
from django.urls import reverse
from tests.users.factories import UserFactory

from hasker.contrib.qa.models import Answer, Question, Tag


class TestQAQuestionsListView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.question2 = Question.objects.create(title="Title2", text='text2', author=cls.user, is_active=True)
        cls.question3 = Question.objects.create(title="Title3", text='text2', author=cls.user, is_active=False)

    @staticmethod
    def questions_list_url():
        return reverse('home')

    def test_questions_list_view(self):
        url = self.questions_list_url()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/home.html')
        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question.pk, self.question2.pk],
            lambda i: i.pk, ordered=False,
        )


class TestQAQuestionDetails(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.answer = Answer.objects.create(text='text', author=cls.user, question=cls.question)
        cls.answer2 = Answer.objects.create(text='text2', author=cls.user, question=cls.question)

    @staticmethod
    def question_details_url(slug):
        return reverse('qa:question-detail', kwargs={"slug": slug})

    def test_questions_list_view(self):
        url = self.question_details_url(self.question.slug)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'qa/question_detail.html')
        self.assertEqual(response.context['object'], self.question)
        self.assertQuerysetEqual(
            response.context['answers'],
            [self.answer.pk, self.answer2.pk],
            lambda i: i.pk, ordered=False,
        )


class TestQAQuestionCreateView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.user = UserFactory()

    @staticmethod
    def question_add_url():
        return reverse('qa:question-add')

    @staticmethod
    def question_details_url(slug):
        return reverse('qa:question-detail', kwargs={"slug": slug})

    def test_question_create_view_not_authorized(self):
        url = self.question_add_url()
        response = self.client.get(url)
        self.assertRedirects(response, '/accounts/login/?next=' + self.question_add_url())

    def test_question_create_view(self):
        self.client.login(username='login', password='TeStPaSS12345')

        url = self.question_add_url()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'qa/question_create.html')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'qa/question_create.html')
        self.assertFormError(response, 'form', 'text', 'Это поле обязательно.')
        self.assertFormError(response, 'form', 'title', 'Это поле обязательно.')
        self.assertFormError(response, 'form', 'tags', 'Это поле обязательно.')

        data = {
            'title': 'question title',
            'text': 'question text',
            'tags': 'tag1, tag2, tag3',
        }

        response = self.client.post(url, data=data)
        obj = Question.objects.latest()
        self.assertRedirects(response, self.question_details_url(obj.slug))
        self.assertEquals(obj.title, 'question title')
        self.assertEquals(obj.text, 'question text')
        self.assertEquals(len(obj.tags.all()), 3)


class TestQAAnswerAddView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)

    @staticmethod
    def answer_add_url(question_id):
        return reverse('qa:answer-add', kwargs={"id": question_id})

    @staticmethod
    def question_details_url(slug):
        return reverse('qa:question-detail', kwargs={"slug": slug})

    def test_answer_add_view_not_authorized(self):
        url = self.answer_add_url(self.question.pk)
        response = self.client.post(url)
        self.assertRedirects(response, '/accounts/login/?next=' + self.answer_add_url(self.question.pk))

    def test_answer_add_view_no_question(self):
        self.client.login(username='login', password='TeStPaSS12345')

        url = self.answer_add_url(999)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 404)

    @patch('hasker.contrib.qa.tasks.sent_answer_add_notification.delay')
    def test_answer_add_view(self, notification_task):

        self.client.login(username='login', password='TeStPaSS12345')
        url = self.answer_add_url(self.question.pk)
        response = self.client.post(url, data={'text': 'answer text'})

        self.assertRedirects(response, self.question_details_url(self.question.slug))

        answer = Answer.objects.latest()

        notification_task.assert_called_once_with(answer.pk, self.question.pk)


class TestQAAnswerQuestionCorrectView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.answer = Answer.objects.create(text='text', author=cls.user, question=cls.question)
        cls.answer2 = Answer.objects.create(text='text2', author=cls.user, question=cls.question)

    @staticmethod
    def answer_correct_url(question_id, answer_id):
        return reverse('qa:correct-answer', kwargs={'question_id': question_id, 'answer_id': answer_id})

    @staticmethod
    def question_details_url(slug):
        return reverse('qa:question-detail', kwargs={"slug": slug})

    def test_answer_correct_not_allow_method(self):
        self.client.login(username='login', password='TeStPaSS12345')
        url = self.answer_correct_url(self.question.pk, self.answer.pk)
        response = self.client.get(url)

        self.assertEquals(response.status_code, 405)

    def test_answer_correct(self):
        self.assertFalse(self.answer.is_correct)
        self.assertFalse(self.answer2.is_correct)
        self.client.login(username='login', password='TeStPaSS12345')
        url = self.answer_correct_url(self.question.pk, self.answer.pk)
        response = self.client.post(url)
        self.assertRedirects(response, self.question_details_url(self.question.slug))

        self.answer.refresh_from_db()
        self.answer2.refresh_from_db()
        self.question.refresh_from_db()
        # invalidate cache
        del self.answer.is_correct
        del self.answer2.is_correct
        self.assertTrue(self.answer.is_correct)
        self.assertFalse(self.answer2.is_correct)
        self.assertEquals(self.question.correct_answer, self.answer)


class TestQARatingChangeView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)

    @staticmethod
    def rating_change_url(app_label, model_name, object_id, action):
        return reverse(
            'qa:rating',
            kwargs={
                'app_label': app_label, 'model_name': model_name,
                'object_id': object_id, 'action': action,
            },
        )

    def test_rating_change_not_ajax_method(self):
        self.client.login(username='login', password='TeStPaSS12345')

        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, 1,
        )
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

        response = self.client.post(url)
        self.assertEquals(response.status_code, 404)

    def test_rating_change_not_allow_method(self):
        self.client.login(username='login', password='TeStPaSS12345')

        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, 1,
        )
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 405)

        response = self.client.delete(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 405)

    def test_rating_change_not_allow_success(self):
        self.client.login(username='login', password='TeStPaSS12345')

        # invalidate cache
        del self.question.rating_sum
        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, 1,
        )
        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 200)
        self.question.refresh_from_db()
        self.assertEquals(self.question.rating_sum, 1)

        del self.question.rating_sum
        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, -1,
        )
        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 200)
        self.question.refresh_from_db()
        self.assertEquals(self.question.rating_sum, 0)

        del self.question.rating_sum
        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, -1,
        )
        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 200)

        self.question.refresh_from_db()
        self.assertEquals(self.question.rating_sum, -1)

    def test_rating_change_not_allow_bad_request(self):
        self.client.login(username='login', password='TeStPaSS12345')

        url = self.rating_change_url(
            self.question._meta.app_label, self.question._meta.model_name,
            self.question.pk, 0,
        )
        response = self.client.post(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 400)
        self.question.refresh_from_db()
        self.assertEquals(self.question.rating_sum, 0)


class TestQASearchListView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.tag = Tag.objects.create(name='tag1')
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.question.tags.add(cls.tag)

        cls.question2 = Question.objects.create(title="Title 2", text='text 2', author=cls.user, is_active=True)
        cls.question3 = Question.objects.create(title="Title 3", text='text 3', author=cls.user, is_active=False)

    @staticmethod
    def search_url():
        return reverse('qa:search')

    def test_search_by_title(self):
        url = self.search_url()

        response = self.client.get(url, {'search': 'Title'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/search.html')

        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question.pk, self.question2.pk],
            lambda i: i.pk, ordered=False,
        )

        response = self.client.get(url, {'search': 'Title 2'})
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question2.pk],
            lambda i: i.pk,
        )

        response = self.client.get(url, {'search': 'Title 3'})
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [])

    def test_search_by_text(self):
        url = self.search_url()

        response = self.client.get(url, {'search': 'text'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/search.html')

        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question.pk, self.question2.pk],
            lambda i: i.pk, ordered=False,
        )

        response = self.client.get(url, {'search': 'text 2'})
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question2.pk],
            lambda i: i.pk,
        )

        response = self.client.get(url, {'search': 'text 3'})
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [])

    def test_search_by_tag(self):
        url = self.search_url()

        response = self.client.get(url, {'search': 'tag:tag1'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/search.html')

        self.assertQuerysetEqual(
            response.context['object_list'],
            [self.question.pk],
            lambda i: i.pk,
        )

    def test_search_by_tag_incorrect(self):
        url = self.search_url()

        response = self.client.get(url, {'search': 'tag:incorrect'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/search.html')
        self.assertQuerysetEqual(response.context['object_list'], [])
