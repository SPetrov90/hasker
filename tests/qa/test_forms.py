from django.test import TestCase

from hasker.contrib.qa.forms import AnswerCreateForm, QuestionCreateForm


class TestQAQuestionCreateForm(TestCase):

    def test_question_form_empty(self):
        required_fields = ('title', 'text', 'tags')

        form = QuestionCreateForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), len(required_fields))
        self.assertIn('Это поле обязательно.', form.errors['title'])
        self.assertIn('Это поле обязательно.', form.errors['text'])
        self.assertIn('Это поле обязательно.', form.errors['tags'])

    def test_question_form_max_length(self):
        form = QuestionCreateForm(data={
            'title': 'a' * 201,
            'text': 'a' * 2001,
            'tags': 'tag1, tag2, tag3, tag4',
        })

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 3)
        self.assertIn(
            'Убедитесь, что это значение содержит не более 200 символов (сейчас 201).',
            form.errors['title'],
        )
        self.assertIn(
            'Убедитесь, что это значение содержит не более 2000 символов (сейчас 2001).',
            form.errors['text'],
        )

        self.assertIn('Не может быть более трех тегов', form.errors['tags'])

    def test_question_form_valid(self):
        form = QuestionCreateForm(data={
            'title': 'Заголовок',
            'text': 'Текст вопроса',
            'tags': 'tag1, tag2, tag3',
        })
        self.assertTrue(form.is_valid())


class TestQAAnswerCreateFormCreateForm(TestCase):

    def test_answer_form_empty(self):
        required_fields = ('text',)
        form = AnswerCreateForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), len(required_fields))
        self.assertIn('Это поле обязательно.', form.errors['text'])

    def test_answer_form_max_length(self):
        form = AnswerCreateForm(data={
            'text': 'a' * 2001,
        })

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)
        self.assertIn(
            'Убедитесь, что это значение содержит не более 2000 символов (сейчас 2001).',
            form.errors['text'],
        )

    def test_answer_form_valid(self):
        form = AnswerCreateForm(data={
            'text': 'Ответ на вопрос',
        })
        self.assertTrue(form.is_valid())
