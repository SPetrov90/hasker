from django.test import TestCase
from django.utils.functional import cached_property
from tests.users.factories import UserFactory

from hasker.contrib.qa.models import Answer, Question, Vote


class QAQuestionTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.question2 = Question.objects.create(title="Title", text='text2', author=cls.user, is_active=True)

    def test_question_get_absolute_url(self):
        self.assertEquals(self.question.get_absolute_url(), f"/qa/{self.question.slug}/")

    def test_question_unique_slug(self):
        self.assertEquals(self.question.get_absolute_url(), f"/qa/{self.question.slug}/")
        self.assertEquals(self.question2.get_absolute_url(), f"/qa/{self.question2.slug}/")
        self.assertNotEqual(self.question.slug, self.question2.slug)

    def test_question_cached_property(self):
        self.assertIsInstance(self.question.__class__.answer_count, cached_property)
        self.assertEquals(self.question.answer_count, 0)
        self.assertEquals(self.question.rating_sum, 0)


class QAAnswerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.answer = Answer.objects.create(text='text', author=cls.user, question=cls.question)
        cls.answer2 = Answer.objects.create(text='text2', author=cls.user, question=cls.question)

    def test_answer_cached_property(self):
        self.assertEquals(self.answer.rating_sum, 0)

    def test_answer_question_answer_count(self):
        self.assertEquals(self.question.answer_count, 2)


class QAVoteTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.question = Question.objects.create(title="Title", text='text', author=cls.user, is_active=True)
        cls.vote = Vote.objects.create(user=cls.user, object_ct=cls.question)

    def test_vote_default(self):
        self.assertEquals(self.vote.vote, 0)
