from typing import Any, Sequence

from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, Faker, post_generation


class UserFactory(DjangoModelFactory):

    username = 'login'
    email = Faker("email")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        self.set_password('TeStPaSS12345')

    class Meta:
        model = get_user_model()
        django_get_or_create = ["username"]
